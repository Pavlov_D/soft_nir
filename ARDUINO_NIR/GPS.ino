#define PI 3.14159265358979323846
#define earthRadiusKm 6371000.0



void gps_setup()
{
  lcd.begin(16, 2);
  lcd.print("gps wait");
  Serial1.begin(9600);

}

void gps_work()
{
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;

  // For one second we parse GPS data and report some key values
  //for (unsigned long start = millis(); millis() - start < 1000;)
  //{

  while (Serial1.available())
  {

    //char c = mySerial.read();
    char c = Serial1.read();
    // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
    if (gps.encode(c)) // Did a new valid sentence come in?
      newData = true;

  }
  //}

  if (newData)
  {

    unsigned long age;
    gps.f_get_position(&flat_gps, &flon_gps, &age);
    satellites_gps = gps.satellites();
    course_gps = gps.course();
    speed_gps = gps.speed();
    hdop_gps = gps.hdop();
    lcd.clear();
    //lcd.setCursor(0, 1);
    //lcd.print(flat_gps == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat_gps, 6);
    lcd.setCursor(0, 0);
    lcd.print(flon_gps == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon_gps, 6);
    lcd.setCursor(11, 1);
    lcd.print(satellites_gps);
    lcd.setCursor(13, 1);
    lcd.print(course_gps);
    lcd.setCursor(0, 1);
    lcd.print(speed_gps);
    lcd.setCursor(3, 1);
    lcd.print(hdop_gps);



  }

}


// This function converts decimal degrees to radians
double deg2rad(double deg) {
  return (deg * PI / 180);
}

//  This function converts radians to decimal degrees
double rad2deg(double rad) {
  return (rad * 180 / PI);
}
//convert degrees to radians
double dtor(double fdegrees)
{
  return (fdegrees * PI / 180);
}

//Convert radians to degrees
double rtod(double fradians)
{
  return (fradians * 180.0 / PI);
}
/**
   Returns the distance between two points on the Earth.
   Direct translation from http://en.wikipedia.org/wiki/Haversine_formula
   @param lat1d Latitude of the first point in degrees
   @param lon1d Longitude of the first point in degrees
   @param lat2d Latitude of the second point in degrees
   @param lon2d Longitude of the second point in degrees
   @return The distance between the two points in kilometers
*/
double distanceLL(double lat1d, double lon1d, double lat2d, double lon2d) {
  double lat1r, lon1r, lat2r, lon2r, u, v;
  lat1r = deg2rad(lat1d);
  lon1r = deg2rad(lon1d);
  lat2r = deg2rad(lat2d);
  lon2r = deg2rad(lon2d);
  u = sin((lat2r - lat1r) / 2);
  v = sin((lon2r - lon1r) / 2);
  return 2.0 * earthRadiusKm * asin(sqrt(u * u + cos(lat1r) * cos(lat2r) * v * v));
}


//Calculate distance form lat1/lon1 to lat2/lon2 using haversine formula
//Note lat1/lon1/lat2/lon2 must be in radians
//Returns distance in feet
long CalcDistance(double lat1, double lon1, double lat2, double lon2)
{
  double dlon, dlat, a, c;
  double dist = 0.0;
  dlon = dtor(lon2 - lon1);
  dlat = dtor(lat2 - lat1);
  a = pow(sin(dlat / 2), 2) + cos(dtor(lat1)) * cos(dtor(lat2)) * pow(sin(dlon / 2), 2);
  c = 2 * atan2(sqrt(a), sqrt(1 - a));

  dist = 6378140 * c;  //radius of the earth (6378140 meters) in feet 20925656.2
  return ( (long) dist + 0.5);
}


//Calculate bearing from lat1/lon1 to lat2/lon2
//Note lat1/lon1/lat2/lon2 must be in radians
//Returns bearing in degrees
int CalcBearing(double lat1, double lon1, double lat2, double lon2)
{
  lat1 = dtor(lat1);
  lon1 = dtor(lon1);
  lat2 = dtor(lat2);
  lon2 = dtor(lon2);

  //determine angle
  double bearing = atan2(sin(lon2 - lon1) * cos(lat2), (cos(lat1) * sin(lat2)) - (sin(lat1) * cos(lat2) * cos(lon2 - lon1)));
  //convert to degrees
  bearing = rtod(bearing);
  //use mod to turn -90 = 270
  bearing = fmod((bearing + 360.0), 360);
  return (int) bearing + 0.5;
}

void ComputeDestPoint(double lat1, double lon1, double iBear, double iDist, double *lat2, double *lon2)
{
  double bearing = dtor((double) iBear);
  double dist = (double) iDist / 6378140.0;
  lat1 = dtor(lat1);
  lon1 = dtor(lon1);
  *lat2 = asin(sin(lat1) * cos(dist) + cos(lat1) * sin(dist) * cos(bearing));
  *lon2 = lon1 + atan2(sin(bearing) * sin(dist) * cos(lat1), cos(dist) - sin(lat1) * sin(*lat2));
  *lon2 = fmod( *lon2 + 3 * PI, 2 * PI ) - PI;
  *lon2 = rtod( *lon2);
  *lat2 = rtod( *lat2);
}

void point()
{

  float  CurLat = 43 + 3 / 60 + 59.54 / 3600;;
  float CurLon = 131 + 54 / 60 + 46.31 / 3600;
  float r_CurLon;
  float r_CurLat;
  float Bearing = 45; // Bearing of travel
  float r_Bearing;
  float Distance = 3; // km per update
  int Eradius = 637100; // mean radius of the earth


  r_CurLon = radians(CurLon);
  r_CurLat = radians(CurLat);
  r_Bearing = radians(Bearing);
  float DestLat = asin(sin(r_CurLat) * cos(Distance / Eradius) + cos(r_CurLat) * sin(Distance / Eradius) * cos(r_Bearing));
  float DestLon = r_CurLon + atan2(sin(r_Bearing) * sin(Distance / Eradius) * cos(r_CurLat), cos(Distance / Eradius) - sin(r_CurLat) * sin(DestLat));
  DestLon = (DestLon + 3 * PI) / (2 * PI);
  int i = DestLon;
  DestLon = (DestLon - i) * (2 * PI) - PI; // normalise to -180..+180º

  //System.out.printf("starting at a Longitude of %f and a Latitude of %f ",CurLon,CurLat);
  Serial.print("starting at a Longitude of ");
  Serial.print(CurLon, 6);
  Serial.print(" and a Latitude of ");
  Serial.println(CurLat, 6);
  //System.out.printf("if we travel %f km on a bearing of %f degrees ",Distance,Bearing);
  Serial.print("if we travel ");
  Serial.print(Distance, 6);
  Serial.print(" km on a bearing of ");
  Serial.print(Bearing, 6);
  Serial.println(" degrees");
  //System.out.printf("we end up at Longitude of %f and a Latitude of %f ",degrees(DestLon),degrees(DestLat));
  Serial.print("we end up at a Longitude of ");
  Serial.print(degrees(DestLon), 6);
  Serial.print(" and a Latitude of ");
  Serial.println(degrees(DestLat), 6);



}




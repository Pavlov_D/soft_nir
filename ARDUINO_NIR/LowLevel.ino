

double LineAngle(double x1, double y1, double x2, double y2)
{
  double angle = atan2(y2 - y1, x2 - x1) * 180.0 / PI;
  return angle;
}

double error_kurs()
{
  double e =  azimut - angle;

  if (abs(e) > 180)
  {
    if (azimut < angle)
    {
      e = azimut + 360 - angle;
     // Serial.print("1- " );
    }
    else
    {
      e = azimut-360 - angle ;
     // Serial.print("2- " );
    }
  }
  return e;  
}


void move_azimut(int a)
{
  azimut=a;
  /// при включении режима, запоминаем текущее направление и движемся по нему
  if (azimut == -1)
    azimut = angle;

  double e = error_kurs();
//  Serial.print("!-");
//  Serial.print(String(azimut) +" | ");
//  Serial.print(String(angle) +" | ");
//  Serial.println(String(err));

  double kp=1;
  double p=-e*kp;

  serva_move(p);
  motor_move(1010);

  


}

void serva_move(int angle)
{
  angle = constrain(angle, -90, 90);
  if (angle < 0)
    myServo.write(90 + angle * -1);
  if (angle >= 0)
    myServo.write(90 - angle);

  //Serial.println("Serva move " + String(angle) );
}
void low_level_setup()
{

  //pinMode(MOTOR_PIN, OUTPUT);

  myServo.attach(SERVA_PIN);
  myServo.write(90); // открыли захват
  myMotor.attach(MOTOR_PIN);
  // myMotor.write(0); // открыли захват
}
/*
  void Stop()
  {
  analogWrite(SPEED_1, 0);
  analogWrite(SPEED_2, 0);
  digitalWrite(DIR_1, LOW);
  digitalWrite(DIR_2, LOW);
  digitalWrite(DIR_3, LOW);
  digitalWrite(DIR_4, LOW);
  }
*/
void motor_move(int speed_motor)
{
  //Serial.println("Move " + String(speed_motor) );
  myMotor.writeMicroseconds(speed_motor);


}



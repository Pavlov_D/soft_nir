#include <Servo.h>
#include "QList.h"
#include "QList.cpp"
#include "arduino_nir.h"
#include "TinyGPS.h"
#include <SoftwareSerial.h>
#include <LiquidCrystal.h>

Servo myServo;
Servo myMotor;

//SoftwareSerial dbgTerminal(PIN_STOROG_RX, PIN_STOROG_TX); // RX, TX
LiquidCrystal lcd(4, 5, 6, 7, 8, 9);

TinyGPS gps;

//SoftwareSerial mySerial = SoftwareSerial(3, 2);





QList<String> myList;

//String str_list[100];
int count_str = 0;

double start = millis();
unsigned long timer = 0;
bool m = false;
bool start_connect = false;
String inputStr = "";
unsigned long timer_storog = 0;
int count_tmp = 0;
double angle = 0; // переменная угл с компаса {-180;180}
double channel_1 = 0;
double channel_2 = 0;
double channel_3 = 0;
double azimut = -1;
int flag_internet = 0;
double vcc=0;


// наши координаты
float flat_gps = 0; //131.889236;
float flon_gps = 0; //43.130210;
unsigned short satellites_gps=0;
unsigned long course_gps=0;
unsigned long speed_gps=0;
long altitude_gps = 0;
unsigned long hdop_gps=0;


void setup() {
  lcd_setup();
  communication_setup();
  rc_setup();
  low_level_setup();
  storog_setup();
  setup_compas();
  gps_setup();

  //Serial.println("Setup OK" );
}

void loop() {

  comm_work();
  //Serial.print("comm_work, " );
  rc_work();
  //Serial.print("rc_work, " );
  storog_work();
  //Serial.print("storog_work, " );
  compas_work();
  //Serial.print("compas_work, " );
  gps_work();
  //Serial.println("gps_work, " );



  /// ручной режим
  if (channel_3 < 1300 && channel_3 > 900)
    rc_manual();

  if (channel_3 > 1300 && channel_3 < 1600)
    move_azimut(azimut);



  lcd.setCursor(11, 0);
  lcd.print(flag_internet);
  lcd.setCursor(13, 0);
  lcd.print(angle);


  /*
    // тест жпс функций
      double d =  distanceLL( 43+3/60+59.89/3600, 131+54/60+45.39/3600, 43+3/60+36.40/3600, 131+54/60+9.17/3600);
      long d2 =  CalcDistance( 43+3/60+48.76/3600, 131+54/60+41.98/3600, 43+3/60+36.40/3600, 131+54/60+9.17/3600);


      Serial.println(d);
      Serial.println(d2);

      double a = CalcBearing(43+3/60+59.89/3600, 131+54/60+45.39/3600, 43+3/60+36.40/3600, 131+54/60+9.17/3600);
      double a2 = CalcBearing(43+4/60+20.27/3600, 131+55/60+19.12/3600, 43+3/60+59.89/3600, 131+54/60+45.39/3600);
      Serial.println(a);
      Serial.println(a2);

      double l1=0;
      double l2=0;
      ComputeDestPoint( 43+3/60+36.40/3600, 131+54/60+9.17/3600, 25, 100, &l1, &l2);
      Serial.println(String(l1));
      Serial.println(String(l2));

      point();
  */

  //Serial.println(String(angle) +" "+String( sin((180 -angle)/(180/PI)) *(180/PI) ) );
  //serva_move(angle);
  //while (true);

  /*
         /// для теста сторожа
      if(count_tmp>20)
        {
          storog_reset_all();
          count_tmp=0;

        }
  */

}




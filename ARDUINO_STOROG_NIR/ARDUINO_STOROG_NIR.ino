int timer = 0;

#include <SoftwareSerial.h>
SoftwareSerial dbgTerminal(12, 11); // RX, TX

#define BUFFER_SIZE 128

int count = 0;
char buffer[BUFFER_SIZE];

void clearBuffer(void) {
  for (int i = 0; i < BUFFER_SIZE; i++ ) {
    buffer[i] = 0;
  }
}


void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  Serial.begin(115200);
  dbgTerminal.begin(9600); // Serial monitor
}


void reset() {
  // тут отрубаем реле на заданный промежуток
  /// пока просто зажигаю светодиод

  digitalWrite(13, HIGH);
  delay(1000);
  digitalWrite(13, LOW);
}
void loop() {


  dbgTerminal.readBytesUntil('\n', buffer, BUFFER_SIZE);

  Serial.println(buffer);
  //int kol = dbgTerminal.read();
  if (strncmp(buffer, "1", 1) == 0)
  {
    Serial.println("OK");
    // обнуляем счетчик
    count=0;
    
  }
  if (strncmp(buffer, "RESET,", 5) == 0)
  {
    Serial.println("RESET!!!");
    reset();

  }
  clearBuffer();
  delay(1000);
  count++;

  if (count > 300) // 5 минут  нет отклика. принудительный ресет
  {
    reset();
    count=0;

  }
  Serial.println(count);




}

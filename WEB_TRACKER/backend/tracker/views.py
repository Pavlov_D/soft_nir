# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render
#from sklearn.externals.six.moves.urllib import request

from tracker import models
from django.http import JsonResponse

# Create your views here.


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def tracker(request):
    position = models.Position.objects.all()
    context = {
        "position": position,
        "client_ip" : get_client_ip(request)
    }

    return render(request, 'tracker.html', context)


def save_page(request):
    loc_count = models.Position.objects.count()

    context = {
        "count": loc_count,
    }
    return render(request, 'save.html', context)


def delete_page(request):
    loc_count = models.Position.objects.count()

    context = {
        "count": loc_count,
    }
    return render(request, 'delete.html', context)


def delete_all_position(request):
    models.Position.objects.all().delete()
    response = {
        'status': 'ok'
    }
    return JsonResponse(response)


def save_new_position(request):
    longitude = 0
    latitude = 0
    if request.method == 'GET':
        longitude = request.GET['longitude']
        latitude = request.GET['latitude']
    if request.method == 'POST':
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
    pos = models.Position()
    pos.longitude = longitude
    pos.latitude = latitude
    pos.save()
    response = {
        'status': 'ok'
    }
    return JsonResponse(response)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.db import models
from django import forms
# Create your models here.

def get_image_path(instance, filename):
    return os.path.join('photo/', filename)


class Position(models.Model):
    photo = models.ImageField(blank=True, upload_to=get_image_path)
    longitude = models.FloatField()
    latitude = models.FloatField()
    created_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"


class PositionForm(forms.Form):
    photo = models.ImageField(blank=True, upload_to=get_image_path)
    longitude = models.FloatField()
    latitude = models.FloatField()

